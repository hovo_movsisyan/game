﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Balance.Content = "$" + Game.balance;
            bamount.Content = "Betting amount - $" + Game.decrement;

        }

        private void BtnClick_Click(object sender, RoutedEventArgs e)
        {
            if(Game.balance <= 0)
            {
                MessageBox.Show("INSUFFICIENT FUNDS");
                return;
            }
            if (Game.attempts == 4)
            {
                MessageBox.Show("GOOD BYE");
                return;
            }
            (int a, int b, int c) numbers = Game.GetRandomNumbers();
            num1.Content = numbers.a;
            num2.Content = numbers.b;
            num3.Content = numbers.c;

            if(numbers.a!=numbers.b || numbers.b != numbers.c)
            {

                Game.balance -= Game.decrement ;
                Balance.Content = Game.balance;
                Game.attempts++;
               // MessageBox.Show("TRY AGAIN");
            }
            else
            {
                Game.balance += Game.decrement * 3;
                Balance.Content = Game.balance;
                Game.attempts = 0;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            int selected = (comboBox.SelectedIndex + 1) * 5;
            bamount.Content = "Betting amount - $" + selected;
            Game.decrement = selected;
        }
    }
}
