﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    class Game
    {
        public static int balance { get; set; }
        public static int decrement { get; set; } = 5;

        public static int attempts { get; set; } = 0;
        static Game()
        {
            balance = 100;
        }

        public static (int, int, int) GetRandomNumbers()
        {
            Random r = new Random();

            (int a, int b, int c) T = (r.Next(0, 2), r.Next(0, 2), r.Next(0, 2));

            return T;
        }

    }
}
